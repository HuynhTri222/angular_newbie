import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/common/services/data.service';

@Component({
  selector: 'app-dang-ky',
  templateUrl: './dang-ky.component.html',
  styleUrls: ['./dang-ky.component.scss']
})
export class DangKyComponent implements OnInit {

  defaultValue = {
    taiKhoan: "huynhtri",
    matKhau: "123",
    email: "huynhtri222@gmail.com"
  }
  constructor(private dataService: DataService) { }

  ngOnInit(): void {
  }

  DangKy(formValue){
    // let user = {
    //   "taiKhoan": thamSo[0],
    //   "matKhau": thamSo[1],
    //   "email": thamSo[2],
    //   "soDt": thamSo[3],
    //   "maNhom": "GP01",
    //   "maLoaiNguoiDung": "KhachHang",
    //   "hoTen": thamSo[4],
    // };
    let user = {
      "taiKhoan": formValue.taiKhoan,
      "matKhau": formValue.matKhau,
      "email": formValue.email,
      "soDt": formValue.soDienThoai,
      "maNhom": "GP01",
      "maLoaiNguoiDung": "KhachHang",
      "hoTen": formValue.hoTen,
    };
    this.dataService.dangKyUser(user).subscribe((data:any) => {
      alert("Dang Ky Thanh Cong!");
    },
      (err:any) => {}
    )
  }
  
}
