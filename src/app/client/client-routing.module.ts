import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChiTietPhimComponent } from './chi-tiet-phim/chi-tiet-phim.component';
import { ClientComponent } from './client.component';
import { DangKyComponent } from './dang-ky/dang-ky.component';
import { DangNhapComponent } from './dang-nhap/dang-nhap.component';
import { DanhSachPhimComponent } from './danh-sach-phim/danh-sach-phim.component';

const routes: Routes = [
  {
    path: "", component: ClientComponent,
    children: [
      // { path: "", component: ClientComponent },
      {
        path: "danh-sach-phim", component: DanhSachPhimComponent,
      },
      {
        path: "chi-tiet/:id", component: ChiTietPhimComponent,
      },
      {
        path: "chi-tiet", component: ChiTietPhimComponent,
      },
      {
        path: "dang-ky", component: DangKyComponent,
      },
      { path: "dang-nhap", component: DangNhapComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
