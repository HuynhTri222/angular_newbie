import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { DataService } from 'src/app/common/services/data.service';
import { ItemPhimComponent } from '../item-phim/item-phim.component';

@Component({
  selector: 'app-danh-sach-phim',
  templateUrl: './danh-sach-phim.component.html',
  styleUrls: ['./danh-sach-phim.component.scss']
})
export class DanhSachPhimComponent implements OnInit {

  //Bat nhieu componet - query list
  @ViewChildren(ItemPhimComponent) tagListItemPhim: QueryList<ItemPhimComponent>;

  danhSachPhim;
  // danhSachPhim = [
  //   {
  //     maPhim: 1,
  //     hinhAnh: "assets/images/hinh1.png",
  //     tenPhim: "abc",
  //     gia: "5000",
  //     soLike: 0,
  //   },
  //   {
  //     maPhim: 2,
  //     hinhAnh: "assets/images/hinh1.png",
  //     tenPhim: "testPhim",
  //     gia: "5000",
  //     soLike: 0,
  //   },
  //   {
  //     maPhim: 3,
  //     hinhAnh: "assets/images/hinh1.png",
  //     tenPhim: "testPhim",
  //     gia: "5000",
  //     soLike: 0, 
  //   },
  //   {
  //     maPhim: 4,
  //     hinhAnh: "assets/images/hinh1.png",
  //     tenPhim: "testPhim",
  //     gia: "5000",
  //     soLike: 0,
  //   },
  // ];

  // danhSachLike = [
  //   { maPhim: 1, tenPhim: "testPhim", soLike: 0 },
  //   { maPhim: 2, tenPhim: "testPhim", soLike: 0 },
  //   { maPhim: 3, tenPhim: "testPhim", soLike: 0 },
  //   { maPhim: 4, tenPhim: "testPhim", soLike: 0 },
  // ];

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    // this.danhSachPhim = this.dataService.danhSachPhim;
    this.dataService.getListMovie().subscribe((res) => {
      this.danhSachPhim = res;
    });
  }

  thich(phim) {
    const movie = this.danhSachPhim.find(item => {
      return item.maPhim === phim.maPhim;
    });
    movie.soLike++;
  }

  themPhim(phim) {
    this.danhSachPhim.push(phim);
  }

  viewChildren() {
    this.tagListItemPhim.map(item => {
      item.phim.gia = 10000;
    });
  }
}
