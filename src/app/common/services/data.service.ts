import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { tap, catchError, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  danhSachPhim = [
    {
      maPhim: 1,
      hinhAnh: "assets/images/hinh1.png",
      tenPhim: "abc",
      gia: "5000",
      soLike: 0,
    },
    {
      maPhim: 2,
      hinhAnh: "assets/images/hinh1.png",
      tenPhim: "testPhim",
      gia: "5000",
      soLike: 0,
    },
    {
      maPhim: 3,
      hinhAnh: "assets/images/hinh1.png",
      tenPhim: "testPhim",
      gia: "5000",
      soLike: 0, 
    },
    {
      maPhim: 4,
      hinhAnh: "assets/images/hinh1.png",
      tenPhim: "testPhim",
      gia: "5000",
      soLike: 0,
    },
  ];

  constructor(private http: HttpClient) { }

  getListMovie(): Observable<any> {
    const url = `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01`;
    return this.http.get(url).pipe(
      tap((data) => {
        return
      }),
      catchError(err => {
        return this.handleErr(err);
      })
    );
  }

  handleErr(err){
    switch (err.status) {
      case 500:
        alert(err.error)
        break;
    
      default:
        break;
    }
    return throwError(err);
  }

  getMovieDetail(maPhim):Observable<any> {
    const url = `https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`;
    return this.http.get(url).pipe(
      tap((data) => {
        return
      }),
      catchError(err => {
        return this.handleErr(err);
      })
    );
  }

  dangKyUser(user):Observable<any> {
    const url = `https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangKy`;
    return this.http.post(url, user).pipe(
      tap((data) => {
        console.log(data);
      } ),
      catchError(err => {
        return this.handleErr(err);
      })
    );
  }
}
