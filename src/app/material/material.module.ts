import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import { CardFancyExampleComponent } from './card-fancy-example/card-fancy-example.component';

@NgModule({
  declarations: [CardFancyExampleComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
  ],
  exports: [
    CardFancyExampleComponent,
  ],
})
export class MaterialModule { }
