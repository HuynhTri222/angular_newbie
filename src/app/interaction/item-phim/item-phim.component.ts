import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-item-phim',
  templateUrl: './item-phim.component.html',
  styleUrls: ['./item-phim.component.scss']
})
export class ItemPhimComponent implements OnInit {

  //tu ngoai truyen vao
  @Input() phim;
  //truyen ra ben ngoai
  @Output() eventPhim = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    // console.log(this.phim);
  }

  like() {
    this.eventPhim.emit(this.phim);
  }

}
