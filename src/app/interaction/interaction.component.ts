import { Component, OnInit, ViewChild } from '@angular/core';
import { DanhSachPhimComponent } from './danh-sach-phim/danh-sach-phim.component';

@Component({
  selector: 'app-interaction',
  templateUrl: './interaction.component.html',
  styleUrls: ['./interaction.component.scss']
})
export class InteractionComponent implements OnInit {

  // static : True to resolve query results before change detection runs, false to resolve after change detection. Defaults to false.
  //bat 1 component
  @ViewChild(DanhSachPhimComponent, { static: false })tagDanhSachPhim: DanhSachPhimComponent;
  constructor() { }

  ngOnInit(): void {
  }

  addMovie(maPhim, tenPhim, giaPhim, hinhAnh) {
    const phim = {
      maPhim,
      tenPhim,
      giaPhim,
      hinhAnh,
      soLike: 0,
    }
    this.tagDanhSachPhim.themPhim(phim);
  }
}
