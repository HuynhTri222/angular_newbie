import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DirectiveComponent } from './directive.component';
import { StructuralDirectiveComponent } from './structural-directive/structural-directive.component';
import { NgIfComponent } from './structural-directive/ng-if/ng-if.component';
import { NgForComponent } from './structural-directive/ng-for/ng-for.component';
import { NgSwitchComponent } from './structural-directive/ng-switch/ng-switch.component';
import { AttributeDirectivesComponent } from './attribute-directives/attribute-directives.component';
import { NgClassComponent } from './attribute-directives/ng-class/ng-class.component';
import { NgStyleComponent } from './attribute-directives/ng-style/ng-style.component';
import { HighLightDirective } from './high-light.directive';



@NgModule({
  declarations: [DirectiveComponent, StructuralDirectiveComponent, NgIfComponent, NgForComponent, NgSwitchComponent, AttributeDirectivesComponent, NgClassComponent, NgStyleComponent, HighLightDirective],
  imports: [
    CommonModule
  ],
  exports: [DirectiveComponent]
})
export class DirectiveModule { }
