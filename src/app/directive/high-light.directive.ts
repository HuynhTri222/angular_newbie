import { Directive, ElementRef, HostBinding, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appHighLight]'
})
export class HighLightDirective {

  constructor(private ele: ElementRef, private render2: Renderer2) { 
    this.ele.nativeElement.style.backgroundColor = "aqua";
    this.render2.setStyle(this.ele.nativeElement, "background-color", "red");
  }
  //gia tri mac dinh khi vua moi render ra, se overlap constructor
  @HostBinding("style.backgroundColor") bgColor: string = "pink";

  //ham bat su kien
  @HostListener("mouseenter") SuKienHover() {
    this.ele.nativeElement.style.backgroundColor = "yellow";
    // this.render2.setStyle(this.ele.nativeElement, "background-color", "red");
  }
  @HostListener("mouseleave") SuKienLeave() {
    this.render2.setStyle(this.ele.nativeElement, 'background-color', "blue");
  }

}
