import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-for',
  templateUrl: './ng-for.component.html',
  styleUrls: ['./ng-for.component.scss']
})
export class NgForComponent implements OnInit {

  danhSachNhanVien = [
    {
      ten: "Tri",
      tuoi: "17"
    },
    {
      ten: "Tri A",
      tuoi: "18"
    },
    {
      ten: "Tri B",
      tuoi: "18"
    },
    {
      ten: "Tri C",
      tuoi: "27"
    },
  ];

  danhSachSanPham = [];

  constructor() { }

  ngOnInit(): void {
  }

  themSanPham(maSP, tenSP, gia) {
    const sp = {
      maSP,
      tenSP,
      gia,
    }

    this.danhSachSanPham.push(sp);
  }


}
