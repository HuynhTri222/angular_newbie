import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-if',
  templateUrl: './ng-if.component.html',
  styleUrls: ['./ng-if.component.scss']
})
export class NgIfComponent implements OnInit {
  status = true;
  statusLogin = false;
  constructor() { }

  ngOnInit(): void {
  }

  dangXuat() {
    this.statusLogin = false;
  }

  dangNhap() {
    this.statusLogin = true;
  }

}
