import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { DashBoardComponent } from './dash-board/dash-board.component';



@NgModule({
  declarations: [AdminComponent, DashBoardComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
  ],
  exports: [AdminComponent]
})
export class AdminModule { }
