import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientComponent } from './client/client.component';
// import { DemoComponent } from './demo/demo.component';
// import { DanhSachPhimComponent } from './interaction/danh-sach-phim/danh-sach-phim.component';
import { CardFancyExampleComponent } from './material/card-fancy-example/card-fancy-example.component';

const routes: Routes = [
  //Routes theo component
  {
    path: "",
    // component: ClientComponent,
    // component: DemoComponent,
    // component: DanhSachPhimComponent,
    // component: ClientComponent,
    loadChildren: './client/client.module#ClientModule'
  },
  // {
  //   path:"**",
  //   component: CardFancyExampleComponent,
  // },
  {
    path:"client",
    // component: DemoComponent,
    component: ClientComponent,
  },
  {
    path: 'admin', loadChildren: './admin/admin.module#AdminModule'
  },
  //Truong hop nay luon luon de cuoi
  {
    path:"**", component: CardFancyExampleComponent,
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
